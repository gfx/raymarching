// OBJViewer.js (c) 2012 matsuda and itami
// Vertex shader program
var VSHADER_SOURCE = 
  'attribute vec4 a_Position;\n' +
  'attribute vec4 a_Color;\n' +
  'attribute vec4 a_Normal;\n' +
  'uniform mat4 u_MvpMatrix;\n' +
  'uniform mat4 u_NormalMatrix;\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_Position = u_MvpMatrix * a_Position;\n' +
  '  vec3 normal = normalize(vec3(u_NormalMatrix * a_Normal));\n' +
  '  float ambient = 0.2;\n' +
  '  vec3 lightDirection0 = normalize(vec3(-0.35, 0.35, 0.87));\n' +
  '  vec3 lightDirection1 = normalize(vec3(0.35, 0.35, -0.87));\n' +
  '  float nDotL0 = max(dot(normal, lightDirection0), 0.0);\n' +
  '  float nDotL1 = max(dot(normal, lightDirection1), 0.0);\n' +
  '  float mult = nDotL0 + nDotL1 + ambient;\n' +
  '  v_Color = vec4(a_Color.rgb * mult, a_Color.a);\n' +
  '}\n';

// Fragment shader program
var FSHADER_SOURCE =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_FragColor = v_Color;\n' +
  '}\n';

var g_plyDoc = null;      // The information of OBJ file
var g_drawingInfo = null; // The information for drawing 3D model

// Coordinate transformation matrix
var g_modelMatrix = new Matrix4();
var g_mvpMatrix = new Matrix4();
var g_normalMatrix = new Matrix4();

var ANGLE_STEP = 30;   // The increments of rotation angle (degrees)



function webgl_initialize(objfilename, canvasname) {
  // Retrieve <canvas> element
  var canvas = document.getElementById(canvasname);
  canvas.width = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;

  // Get the rendering context for WebGL
  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }

  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log('Failed to intialize shaders.');
    return;
  }

  // Set the clear color and enable the depth test
  gl.clearColor(0.2, 0.2, 0.2, 1.0);
  gl.enable(gl.DEPTH_TEST);

  // Get the storage locations of attribute and uniform variables
  var program = gl.program;
  program.a_Position     = gl.getAttribLocation(program, 'a_Position');
  program.a_Normal       = gl.getAttribLocation(program, 'a_Normal');
  program.a_Color        = gl.getAttribLocation(program, 'a_Color');
  program.u_MvpMatrix    = gl.getUniformLocation(program, 'u_MvpMatrix');
  program.u_NormalMatrix = gl.getUniformLocation(program, 'u_NormalMatrix');

  if (program.a_Position < 0 ||  program.a_Normal < 0 || program.a_Color < 0 ||
      !program.u_MvpMatrix || !program.u_NormalMatrix) {
    console.log('attribute, uniform変数の格納場所の取得に失敗'); 
    return;
  }

  // Prepare empty buffer objects for vertex coordinates, colors, and normals
  var model = initVertexBuffers(gl, program);
  if (!model) {
    console.log('Failed to set the vertex information');
    return;
  }

  // ビュー投影行列を計算
  var viewProjMatrix = new Matrix4();
  viewProjMatrix.setPerspective(30.0, canvas.width / canvas.height, 1.0, 5000.0);
  viewProjMatrix.lookAt(0.0, 200.0, 400.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

  // Start reading the OBJ file
  readPLYFile(objfilename, gl, model, 80, true);

  var currentAngle = 0.0; // Current rotation angle [degree]
  var tick = function() {   // Start drawing
    currentAngle = animate(currentAngle); // Update current rotation angle
    draw(gl, gl.program, currentAngle, viewProjMatrix, model);
    requestAnimationFrame(tick, canvas);
  };
  tick();
}

// Create an buffer object and perform an initial configuration
function initVertexBuffers(gl, program) {
  var o = new Object(); // Utilize Object object to return multiple buffer objects
  o.vertexBuffer = createEmptyArrayBuffer(gl, program.a_Position, 3, gl.FLOAT); 
  o.normalBuffer = createEmptyArrayBuffer(gl, program.a_Normal, 3, gl.FLOAT);
  o.colorBuffer = createEmptyArrayBuffer(gl, program.a_Color, 4, gl.FLOAT);
  o.indexBuffer = gl.createBuffer();
  if (!o.vertexBuffer || !o.normalBuffer || !o.colorBuffer || !o.indexBuffer) { return null; }

  gl.bindBuffer(gl.ARRAY_BUFFER, null);

  return o;
}

// Create a buffer object, assign it to attribute variables, and enable the assignment
function createEmptyArrayBuffer(gl, a_attribute, num, type) {
  var buffer =  gl.createBuffer();  // Create a buffer object
  if (!buffer) {
    console.log('Failed to create the buffer object');
    return null;
  }
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.vertexAttribPointer(a_attribute, num, type, false, 0, 0);  // Assign the buffer object to the attribute variable
  gl.enableVertexAttribArray(a_attribute);  // Enable the assignment

  return buffer;
}


// Read a file
function readPLYFile(fileName, gl, model, scale, reverse) {
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (request.readyState === 4 && request.status !== 404) {
      onReadPLYFile(request.responseText, fileName, gl, model, scale, reverse);
    }
  }
  request.open('GET', fileName, true); // Create a request to acquire the file
  request.send();                      // Send the request
}

// PLY File has been read
function onReadPLYFile(fileString, fileName, gl, o, scale, reverse) {
  var plyDoc = new PLYDoc(fileName);  // Create a PLYDoc object
  var result = plyDoc.parse(fileString, scale, reverse); // Parse the file
  if (!result) {
    g_plyDoc = null; g_drawingInfo = null;
    console.log("PLY file parsing error.");
    return;
  }
  g_plyDoc = plyDoc;
}


function draw(gl, program, angle, viewProjMatrix, model) {
  if (g_plyDoc != null){ // PLY is available
    g_drawingInfo = onReadComplete(gl, model, g_plyDoc);
    g_plyDoc = null;
  }
  if (!g_drawingInfo) return;

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);  // Clear color and depth buffers

  g_modelMatrix.setRotate(angle, 0.0, 1.0, 0.0);
  //g_modelMatrix.rotate(angle, 0.0, 1.0, 0.0);
  //g_modelMatrix.rotate(angle, 0.0, 0.0, 1.0);

  // Calculate the normal transformation matrix and pass it to u_NormalMatrix
  g_normalMatrix.setInverseOf(g_modelMatrix);
  g_normalMatrix.transpose();
  gl.uniformMatrix4fv(program.u_NormalMatrix, false, g_normalMatrix.elements);

  // Calculate the model view project matrix and pass it to u_MvpMatrix
  g_mvpMatrix.set(viewProjMatrix);
  g_mvpMatrix.multiply(g_modelMatrix);
  gl.uniformMatrix4fv(program.u_MvpMatrix, false, g_mvpMatrix.elements);

  // Draw
  gl.drawElements(gl.TRIANGLES, g_drawingInfo.indices.length, gl.UNSIGNED_SHORT, 0);
}

// OBJ File has been read completely
function onReadComplete(gl, model, plyDoc) {
  // Acquire the vertex coordinates and colors from OBJ file
  var drawingInfo = plyDoc.getDrawingInfo();

  // Write date into the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, model.vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.vertices, gl.STATIC_DRAW);

  gl.bindBuffer(gl.ARRAY_BUFFER, model.normalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.normals, gl.STATIC_DRAW);
  
  gl.bindBuffer(gl.ARRAY_BUFFER, model.colorBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.colors, gl.STATIC_DRAW);
  
  // Write the indices to the buffer object
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, drawingInfo.indices, gl.STATIC_DRAW);

  return drawingInfo;
}

var last = Date.now(); // Last time that this function was called
function animate(angle) {
  var now = Date.now();   // Calculate the elapsed time
  var elapsed = now - last;
  last = now;
  // Update the current rotation angle (adjusted by the elapsed time)
  var newAngle = angle + (ANGLE_STEP * elapsed) / 1000.0;
  return newAngle % 360;
}

//------------------------------------------------------------------------------
// OBJParser
//------------------------------------------------------------------------------

// PLYDoc object
// Constructor
var PLYDoc = function(fileName) {
  this.fileName = fileName;
  this.vertices = new Array(0);  // Initialize the property for Vertex
  this.normals  = new Array(0);  // Initialize the property for Normal
  this.colors   = new Array(0);  // Initialize the property for Color
  this.vIndices = new Array(0);
}

// Parsing the OBJ file
PLYDoc.prototype.parse = function(fileString, scale, reverse) {
  var lines = fileString.split('\n');  // Break up into lines and store them as array
  lines.push(null); // Append null
  var index = 2;    // Initialize index of line

  // Parse line by line
  var line;         // A string in the line to be parsed
  var sp = new StringParser();  // Create StringParser
  
  if(lines[0] != 'ply' || lines[1] != 'format ascii 1.0') {
    console.log("PLY file parsing error.");
    return false;
  }
  
  var vert_count = 0;
  var vert_has_normal = 0;
  var vert_has_uv = 0;
  var face_count = 0;
  
  var mode = 0;
  var is_done = 0;
  
  // parse PLY header
  while(!is_done && (line = lines[index++]) != null) {
    sp.init(line);                  // init StringParser
    var command = sp.getWord();     // Get command
    if(command == null)	 continue;  // check null command
    switch(command) {
      case 'comment':
        continue; // skip
      case 'element':
        var element_type = sp.getWord();
        switch(element_type) {
          case 'vertex':
            vert_count = sp.getInt();
            mode = 1;
            continue;
          case 'face':
            face_count = sp.getInt();
            mode = 2;
            continue;
        }
        continue;
      case 'property':
        if(mode == 1) {
          var prop_type = sp.getWord();
          var prop_var = sp.getWord();
          switch(prop_var) {
            case 'x':
            case 'y':
            case 'z':
              continue;
            case 'nx':
            case 'ny':
            case 'nz':
              vert_has_normal = 1;
              continue;
            case 's':
            case 't':
              vert_has_uv = 1;
              continue;
          }
          continue;
        } else if(mode==2) {
          // assuming simple
          continue;
        }
      case 'end_header':
        is_done=1;
        break;
    }
  }
  
  console.log(index);
  console.log(vert_count);
  for(var ivert=0; ivert<vert_count; ivert++) {
    line = lines[index++];
    if(line == null) {
      console.log(ivert);
      return false;
    }
    sp.init(line);
    var x = sp.getFloat() * scale;
    var y = sp.getFloat() * scale;
    var z = sp.getFloat() * scale;
    this.vertices.push(new Vertex(x,y,z));
    if(ivert<10) console.log(x);
    //console.log(x);
    if(vert_has_normal) {
      var nx = sp.getFloat();
      var ny = sp.getFloat();
      var nz = sp.getFloat();
      this.normals.push(new Normal(nx,ny,nz));
    }
    if(vert_has_uv) {
      var tu = sp.getFloat();
      var tv = sp.getFloat();
      // not handled now :(
    }
  }
  
  console.log(face_count);
  for(var iface=0; iface<face_count; iface++) {
    line = lines[index++];
    if(line == null) return false;
    sp.init(line);
    var face_size = sp.getInt();
    if(face_size == 3) {
      var i0 = sp.getInt();
      var i1 = sp.getInt();
      var i2 = sp.getInt();
      this.vIndices.push(i0);
      this.vIndices.push(i1);
      this.vIndices.push(i2);
    } else if(face_size == 4) {
      var i0 = sp.getInt();
      var i1 = sp.getInt();
      var i2 = sp.getInt();
      var i3 = sp.getInt();
      this.vIndices.push(i0);
      this.vIndices.push(i1);
      this.vIndices.push(i2);
      
      this.vIndices.push(i0);
      this.vIndices.push(i2);
      this.vIndices.push(i3);
    }
  }
  
  return true;
}

// Find color by material name
PLYDoc.prototype.findColor = function(name){
  for(var i = 0; i < this.mtls.length; i++){
    for(var j = 0; j < this.mtls[i].materials.length; j++){
      if(this.mtls[i].materials[j].name == name){
        return(this.mtls[i].materials[j].color)
      }
    }
  }
  return(new Color(0.8, 0.8, 0.8, 1));
}

//------------------------------------------------------------------------------
// Retrieve the information for drawing 3D model
PLYDoc.prototype.getDrawingInfo = function() {
  // Create an arrays for vertex coordinates, normals, colors, and indices
  var numVertices = this.vIndices.length;
  var vertices = new Float32Array(numVertices * 3);
  var normals  = new Float32Array(numVertices * 3);
  var colors   = new Float32Array(numVertices * 4);
  var indices  = new Uint16Array(numVertices);
  
  // Set vertex, normal and color
  var index_indices = 0;
  for(var i = 0; i < numVertices; i++) {
    var i_vert = this.vIndices[i];
    indices[i] = i_vert;
    vertices[i*3+0] = this.vertices[i_vert].x;
    vertices[i*3+1] = this.vertices[i_vert].y;
    vertices[i*3+2] = this.vertices[i_vert].z;
    normals[i*3+0] = this.normals[i_vert].x;
    normals[i*3+1] = this.normals[i_vert].y;
    normals[i*3+2] = this.normals[i_vert].z;
    colors[i*4+0] = 0.7;
    colors[i*4+1] = 0.7;
    colors[i*4+2] = 0.7;
    colors[i*4+3] = 1.0;
  }

  return new DrawingInfo(vertices, normals, colors, indices);
}

//------------------------------------------------------------------------------
// Vertex Object
//------------------------------------------------------------------------------
var Vertex = function(x, y, z) {
  this.x = x;
  this.y = y;
  this.z = z;
}

//------------------------------------------------------------------------------
// Normal Object
//------------------------------------------------------------------------------
var Normal = function(x, y, z) {
  this.x = x;
  this.y = y;
  this.z = z;
}

//------------------------------------------------------------------------------
// Color Object
//------------------------------------------------------------------------------
var Color = function(r, g, b, a) {
  this.r = r;
  this.g = g;
  this.b = b;
  this.a = a;
}

//------------------------------------------------------------------------------
// DrawInfo Object
//------------------------------------------------------------------------------
var DrawingInfo = function(vertices, normals, colors, indices) {
  this.vertices = vertices;
  this.normals  = normals;
  this.colors   = colors;
  this.indices  = indices;
}

//------------------------------------------------------------------------------
// Constructor
var StringParser = function(str) {
  this.str;   // Store the string specified by the argument
  this.index; // Position in the string to be processed
  this.init(str);
}
// Initialize StringParser object
StringParser.prototype.init = function(str){
  this.str = str;
  this.index = 0;
}

// Skip delimiters
StringParser.prototype.skipDelimiters = function()  {
  for(var i = this.index, len = this.str.length; i < len; i++){
    var c = this.str.charAt(i);
    // Skip TAB, Space, '(', ')
    if (c == '\t'|| c == ' ' || c == '(' || c == ')' || c == '"') continue;
    break;
  }
  this.index = i;
}

// Skip to the next word
StringParser.prototype.skipToNextWord = function() {
  this.skipDelimiters();
  var n = getWordLength(this.str, this.index);
  this.index += (n + 1);
}

// Get word
StringParser.prototype.getWord = function() {
  this.skipDelimiters();
  var n = getWordLength(this.str, this.index);
  if (n == 0) return null;
  var word = this.str.substr(this.index, n);
  this.index += (n + 1);

  return word;
}

// Get integer
StringParser.prototype.getInt = function() {
  return parseInt(this.getWord());
}

// Get floating number
StringParser.prototype.getFloat = function() {
  return parseFloat(this.getWord());
}

// Get the length of word
function getWordLength(str, start) {
  var n = 0;
  for(var i = start, len = str.length; i < len; i++){
    var c = str.charAt(i);
    if (c == '\t'|| c == ' ' || c == '(' || c == ')' || c == '"') 
	break;
  }
  return i - start;
}

//------------------------------------------------------------------------------
// Common function
//------------------------------------------------------------------------------
function calcNormal(p0, p1, p2) {
  // v0: a vector from p1 to p0, v1; a vector from p1 to p2
  var v0 = new Float32Array(3);
  var v1 = new Float32Array(3);
  for (var i = 0; i < 3; i++){
    v0[i] = p0[i] - p1[i];
    v1[i] = p2[i] - p1[i];
  }

  // The cross product of v0 and v1
  var c = new Float32Array(3);
  c[0] = v0[1] * v1[2] - v0[2] * v1[1];
  c[1] = v0[2] * v1[0] - v0[0] * v1[2];
  c[2] = v0[0] * v1[1] - v0[1] * v1[0];

  // Normalize the result
  var v = new Vector3(c);
  v.normalize();
  return v.elements;
}
