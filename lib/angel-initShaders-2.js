//
//  LoadShaders.js
//

function initShaders( gl, vertexShaderId, fragmentShaderId, sceneString, numObjs)
{
    var vertShdr;
    var fragShdr;

    var vertElem = document.getElementById( vertexShaderId );
    if ( !vertElem ) { 
        alert( "Unable to load vertex shader " + vertexShaderId );
        return -1;
    }
    else {
        vertShdr = gl.createShader( gl.VERTEX_SHADER );
        gl.shaderSource( vertShdr, vertElem.text );
        gl.compileShader( vertShdr );
        if ( !gl.getShaderParameter(vertShdr, gl.COMPILE_STATUS) ) {
            var msg = "Vertex shader failed to compile.  The error log is:"
        	+ "<pre>" + gl.getShaderInfoLog( vertShdr ) + "</pre>";
            alert( msg );
            return -1;
        }
    }

    var fragElem = document.getElementById( fragmentShaderId );
    if ( !fragElem ) { 
        alert( "Unable to load vertex shader " + fragmentShaderId );
        return -1;
    }
    else {
        fragShdr = gl.createShader( gl.FRAGMENT_SHADER );
        var temp = fragElem.text.replace("GENERATED CODE HERE", sceneString);
        temp = temp.replace("uniform vec3 objPositions[2];", "uniform vec3 objPositions["+numObjs+"];");
        temp = temp.replace("uniform vec4 objColors[2];", "uniform vec4 objColors["+numObjs+"];");        
        temp = temp.replace("uniform mat4 objMats[2];", "uniform mat4 objMats["+numObjs+"];");
        temp = temp.replace("uniform mat4 objNormMats[2];", "uniform mat4 objNormMats["+numObjs+"];");
        
        gl.shaderSource( fragShdr, temp );
        gl.compileShader( fragShdr );
        console.log(temp);
        if ( !gl.getShaderParameter(fragShdr, gl.COMPILE_STATUS) ) {
            console.log(temp);
            var msg = "Fragment shader failed to compile.  The error log is:"
        	+ "<pre>" + gl.getShaderInfoLog( fragShdr ) + "</pre>";
            alert( msg );
            return -1;
        }
    }

    var program = gl.createProgram();
    gl.attachShader( program, vertShdr );
    gl.attachShader( program, fragShdr );
    gl.linkProgram( program );
    
    if ( !gl.getProgramParameter(program, gl.LINK_STATUS) ) {
        var msg = "Shader program failed to link.  The error log is:"
            + "<pre>" + gl.getProgramInfoLog( program ) + "</pre>";
        alert( msg );
        return -1;
    }

    return program;
}
