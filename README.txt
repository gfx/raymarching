Run instructions: Open Main.html in google chrome. A high powered Nvidia gpu is recommended.

This program is a 3D rendering application that I've created over the last two semesters. The JavaScript code provides a platform for creating and rendering a 3D scene. 
It does this by dynamically generating GLSL code which gets run on the fragment shader in order to render the scene using ray marching. Part of this project was creating a 
tree like acceleration structure which greatly reduces the render time per frame.
