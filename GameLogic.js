var bullets = [];
var spentBullets = [];
var bulletCnt = 6;
var reloadTime = 1.0;

function TileToSpace(row, col) {
    var centerx = (row)*mapscale;
    var centery = (col)*mapscale;
    
	return ([centerx, centery]);
}

function SpaceToTile(posX, posZ) {
	var row = Math.round((posX / mapscale));
	var col = Math.round((posZ / mapscale));
	return ([row, col]);
	
}

function CollisionMovement(tpos, rpos, mscale) {
	var oposition = SpaceToTile(rpos[0], rpos[2]);
	var nposition = SpaceToTile(tpos[0], tpos[2]);  
    var newpos = new vec3(tpos[0],tpos[1],tpos[2]);
	var level = level_1.layout;
    var velocity = new vec3(tpos[0]-rpos[0], 0, tpos[2]-rpos[2]);
	//console.log(row, col, level[row][col]);
	if (level[nposition[0]][nposition[1]] == '#' || level[nposition[0]][nposition[1]] == '*')
    {
        var posdif = [nposition[0] - oposition[0], nposition[1] - oposition[1]];
        if(level[oposition[0]+posdif[0]][oposition[1]] == '#' || level[oposition[0]+posdif[0]][oposition[1]] == '*')
        {
            velocity[0] = 0;
        }
        if(level[oposition[0]][oposition[1]+posdif[1]] == '#' || level[oposition[0]][oposition[1]+posdif[1]] == '*')
        {
            velocity[2] = 0;
        }
    } 
    
    var tmscale = mscale/(fps);
    if(velocity[0] || velocity[1] || velocity[2])
        normalize(velocity);

    newpos[0] = rpos[0]+tmscale*velocity[0];
    newpos[1] = rpos[1]+tmscale*velocity[1];
    newpos[2] = rpos[2]+tmscale*velocity[2];
	return newpos;
}

function YeOldeCollisionMovement(posX, posZ) {
	var position = SpaceToTile(posX, posZ);
	var row = position[0];
	var col = position[1];
	var level = level_1.layout;
	//console.log(row, col, level[row][col]);
	if (level[row][col] == '#') return true;
	if (level[row][col] == '*') return true;
	return false;
	
}

function StrictYeOldeCollisionMovement(posX, posZ, ignoreList) {
	var position = SpaceToTile(posX, posZ);
	var row = position[0];
	var col = position[1];
	var level = level_1.layout;
	
	var inList = false;	
	for (var i = 0; i < ignoreList.length; i++)	{
		if (level[row][col] == ignoreList[i])
        {
            inList = true;
            break;
        }
	}
	return (inList != true);	
}

function ParseVecString(istr)
{
    var tstr = istr.split(" ");  
    var tvec = new vec3(parseFloat(tstr[0]), parseFloat(tstr[1]), parseFloat(tstr[2]));
    console.log(tvec);
    return tvec;
}

function CreateRoom(program, modHandle, coors, mapscale, yOffset)
{
        var outScale = 1.1;
        /*coors[0] -= 1;
        coors[1] -= 1;
        coors[2] += 1;
        coors[3] += 1;*/
        var scalex = ((coors[2]-coors[0]))/2;
        var scaley = ((coors[3]-coors[1]))/2;
        var centerx = (coors[0]+scalex)*mapscale;
        var centery = (coors[1]+scaley)*mapscale;
        var contScale = scalex+.5+2*_mapthreshold;
        
        if(scaley > contScale)
            contScale = scaley+.5+2*_mapthreshold;
        
        var pid = screens[0].AddObject(gl, program, objType.SPHERE, 2, vec3(centerx,yOffset, centery), vec4(Math.random(),Math.random(),Math.random(),1.0), 1.6*contScale*mapscale, modHandle);
        //var contid = screens[0].AddObject(gl, program, objType.CUBE, 1, vec3(0.0,0.0, 0.0), vec4(.2,1.0,.5,1.0), 1.0001, modHandle, vec3(scalex*mapscale*outScale, 1.0/outScale, scaley*mapscale*outScale));        
        var id = screens[0].AddObject(gl, program, objType.CUBE, 1, vec3(0.0,yOffset, 0.0), vec4(Math.random(),Math.random(),Math.random(),1.0), 1.0, modHandle, vec3((scalex+.5)*mapscale, 10.0, (scaley+.5)*mapscale));

        screens[0].AddDependency(opType.CHILD, pid, id);
        //screens[0].AddDependency(opType.SUB, contid, id);
        if(coors.length > 4)
        {
        
           //coors[4] -= 1;
            //coors[5] -= 1;
            //coors[6] += 1;
            //coors[7] += 1;     
            scalex = (coors[6]-coors[4])/2;
            scaley = (coors[7]-coors[5])/2;
  
            var ncenterx = (coors[4]+scalex)*mapscale;
            var ncentery = (coors[5]+scaley)*mapscale;
            var cid = screens[0].AddObject(gl, program, objType.CUBE, 0, vec3(ncenterx - centerx,yOffset, ncentery - centery), vec4(Math.random(),Math.random(),Math.random(),1.0), 1.0, modHandle, vec3((scalex+.5)*mapscale, 1.0, (scalex+.5)*mapscale));
            screens[0].AddDependency(opType.SUB, id, cid);
        }    
        return pid;
}

function CreateCooridor(program, modHandle, coors, mapscale, yOffset)
{
        
        var wscalex = (coors[coors.length -1][0]-coors[0][0])/2;
        var wscaley = (coors[coors.length -1][1]-coors[0][1])/2;
        console.log("wscalexy "+wscalex+", "+wscaley);
        var wcenterx = (coors[0][0]+wscalex)*mapscale;
        var wcentery = (coors[0][1]+wscaley)*mapscale;
        console.log("wcenter "+wcenterx+", "+wcentery);
        
        wscalex = Math.abs(wscalex);
        wscaley = Math.abs(wscaley);        
        
        var wscale = wscalex+0.5+_mapthreshold;
        if(wscaley > wscale)
        {
            wscale = wscaley+0.5+_mapthreshold;
        }
        
         console.log("wscale "+wscale);
        
        var pid = screens[0].AddObject(gl, program, objType.SPHERE, 2, vec3(wcenterx,yOffset, wcentery), vec4(Math.random(),Math.random(),Math.random(),1.0), 1.6*wscale*mapscale, modHandle);
        var tuncol = vec4(Math.random(),Math.random(),Math.random(),1.0);
        for(var i = 0; i < coors.length-1; i++)
        {
            var scalex = (coors[i+1][0]-coors[i][0])/2;
            var scaley = (coors[i+1][1]-coors[i][1])/2;
            var centerx = (coors[i][0]+scalex)*mapscale;
            var centery = (coors[i][1]+scaley)*mapscale;
            var travelY = false;
            var cylscale = Math.abs(scalex)+.5+_mapthreshold;
            
            //console.log("Cylscale "+cylscale);            
            
            if(coors[i][0] == coors[i+1][0])
            {
                travelY = true;
                cylscale = Math.abs(scaley)+0.5+_mapthreshold;
            }
                
           //if(i == 0 || i == coors.length-2)
              //  cylscale = cylscale*1.75;
        
            var cid = screens[0].AddObject(gl, program, objType.CYLINDER, 1, vec3(centerx-wcenterx,yOffset, centery-wcentery), tuncol, 1.0, modHandle, vec3( mapscale*(.5+2*_mapthreshold),  cylscale*mapscale, 0.0));
            //console.log("CYLINDER ID!!! " + cid);
            if( travelY == true )
            {
                screens[0].GetObject(cid).Rotate(90, 0);
            }
            else
                screens[0].GetObject(cid).Rotate(90, 2);

            screens[0].AddDependency(opType.CHILD, pid, cid);
            //screens[0].AddDependency(opType.SUB, contid, id);
        }
        //console.log("UPDATED!!!"+screens[0].GetObject(pid));        
        console.log(screens[0].GetObject(pid));       
        return pid;
}

var enemies = [];
var items = [];
function parseLevel(dimensions, program, modHandle) {
	level = level_1.layout;
	
	var outScale = 1.1;
	/*dimensions[0] -= 1;
	dimensions[1] -= 1;
	dimensions[2] += 1;
	dimensions[3] += 1;*/
	var scalex = ((dimensions[2]-dimensions[0]))/2;
	var scaley = ((dimensions[3]-dimensions[1]))/2;
	var centerx = (dimensions[0]+scalex)*mapscale;
	var centery = (dimensions[1]+scaley)*mapscale;
	var contScale = scalex+.5+2*_mapthreshold;
	
	if(scaley > contScale)
		contScale = scaley+.5+2*_mapthreshold;
	

	var parentID = screens[0].AddObject(gl, program, objType.SPHERE, 2, vec3(centerx,0.0, centery), vec4(1,1,1,1.0), 1.6*contScale*mapscale, modHandle);
	
	for (var row = dimensions[0]; row < dimensions[2]+1; row++){
		for (var col = dimensions[1]; col < dimensions[3]+1; col++){
            if(level[row][col] == 'S')
            {
                position = TileToSpace(row, col);
                screens[0].camera.pos = new vec3(position[0], 0.0, position[1]);
            }         
			if (level[row][col] == 'x'  || level[row][col] == 'X'){
				var loc = TileToSpace(row, col);
				var enemyID = screens[0].AddObject(gl, program, objType.SPHERE, 1, vec3(loc[0], 0.0, loc[1]), vec4(.2,.8,1.0,1.0), 0.5, modHandle, vec3(1, 1, 1));
				var enemy = screens[0].GetObject(enemyID);
				
				console.log(screens[0].GetObject(enemyID).position);
				enemy.parent = screens[0].GetObject(parentID);
				enemy.position[0] = enemy.position[0] - screens[0].GetObject(parentID).position[0];
				enemy.position[1] = enemy.position[1] - screens[0].GetObject(parentID).position[1];
				enemy.position[2] = enemy.position[2] - screens[0].GetObject(parentID).position[2];
				console.log(screens[0].GetObject(enemyID).position);
				
				screens[0].AddDependency(opType.CHILD, parentID, enemyID);
				//screens[0].AddDependency(opType.CHILD, 0, enemyID);
				enemy.velocity = new vec3(0,0,0);
				
				enemies.push(enemyID);
				if (level[row][col] == 'X') {
					enemy.isMoving = true;
					setDirection(enemyID, [row, col], dimensions);
					
					var speed = 1 / 60 ;
					if (enemy.direction == 0) enemy.velocity[2] = speed*mapscale/avgFrameTime;
					if (enemy.direction == 2) enemy.velocity[0] = speed*mapscale/avgFrameTime;
				}
				else enemy.isMoving = false;
			}
			if (level[row][col] == '+') items.push([row, col]);
			if (level[row][col] == '?') items.push([row, col]);
		}
	}
	return parentID;
}

function setDirection(enemyID, enemyPos, dimensions){
	var enemy = screens[0].GetObject(enemyID);
	var vertWall = Math.max(Math.abs(enemyPos[1] - dimensions[1]) , Math.abs(enemyPos[1]-dimensions[3]));
	var horzWall = Math.max(Math.abs(enemyPos[0] - dimensions[0]) , Math.abs(enemyPos[0]-dimensions[2]));
	if (vertWall < horzWall) enemy.direction = 2;
	else if (horzWall < vertWall) enemy.direction = 0;
	else enemy.direction = 0;
	
}

function LoadLevel(program, modHandle)
{
    var mapx = level_1.layout[0].length;
    var mapy = level_1.layout.length;
    
    var yOffset = 0.0;
    
    screens[0].AddObject(gl, program, objType.CUBE, 1, vec3((mapx/2)*mapscale,yOffset, (mapy/2)*mapscale), vec4(.2,.8,1.0,1.0), 1.0, modHandle, vec3((mapx/2)*mapscale, 3.0, (mapy/2)*mapscale));
    screens[0].AddDependency(opType.CHILD, 0, 1); 
    
    for(var i = 0; i < level_1.rooms.length; i++)
    {
        /*if(i % 3 == 0)
            var nid = */
        var cid = CreateRoom(program, modHandle, level_1.rooms[i], mapscale, yOffset);
        screens[0].AddDependency(opType.SUB, 1, cid);
		var eid = parseLevel(level_1.rooms[i], program, modHandle);    		
        screens[0].AddDependency(opType.CHILD, 0, eid);
    }
    
    for(var i = 0; i < level_1.corridors.length; i++)
    {
        var cid = CreateCooridor(program, modHandle, level_1.corridors[i], mapscale, yOffset);
        screens[0].AddDependency(opType.SUB, 1, cid);
        //screens[0].AddDependency(opType.CHILD, 0, cid);
    }
    //
	//PLACE THE PLAYER
    
    while (bullets.length < bulletCnt) {
		var sphereID = screens[0].AddObject(gl, program, objType.SPHERE, 1, vec3(screens[0].camera.pos[0], screens[0].camera.pos[1]-2.0, screens[0].camera.pos[2]), vec4(.8,.8,.2,1.0), 0.1, modHandle, vec3(1, 1, 1));
		screens[0].AddDependency(opType.SPHERE, 0, sphereID);
		sphereID.velocity = new vec3(0,0,0);
		bullets.push(sphereID);	
	}
}


function HandleEnemies(){
	document.getElementById('targets').innerHTML = 'Targets: ' + enemies.length;
	if (enemies.length == 0){
		return true;
	}
	else {
		for (var i = 0; i < enemies.length; i++){
			var enemy = screens[0].GetObject(enemies[i]);
			if (enemy.isMoving == true){
				for (var attempts = 0; attempts < 10; attempts++){
					enemy.position[0] += enemy.velocity[0];
					enemy.position[2] += enemy.velocity[2];
					if (StrictYeOldeCollisionMovement(enemy.position[0]+enemy.parent.position[0], enemy.position[2]+enemy.parent.position[2], ['.', 'X']) == true){
						enemy.velocity[0] = -enemy.velocity[0];
						enemy.velocity[2] = -enemy.velocity[2];
					}
					else break;
				}
			}
		}		
	}
}


var current_TDIR = new vec3(0,0,0);
function updateBullets() {
	var currentTime = new Date();
	if (reloadTimer != false){
		if (currentTime.getTime()-reloadTimer.getTime() > reloadTime*1000) return reloadBullets();
	}
	
	var bulletID = -1;
	var bullet = false;
	for (var i = 0; i < spentBullets.length; i++){
		bulletID = spentBullets[i];
		bullet = screens[0].GetObject(bulletID);
		if (bullet.velocity){
			bullet.position[0] += bullet.velocity[0];
			bullet.position[1] += bullet.velocity[1];
			bullet.position[2] += bullet.velocity[2];		
		}
		
		if (bullet.position[1] < screens[0].camera.pos[1]+0.5)
		if (YeOldeCollisionMovement(bullet.position[0], bullet.position[2])){
			bullet.position[0] = screens[0].camera.pos[0];
			bullet.position[1] = screens[0].camera.pos[1]-2.0;
			bullet.position[2] = screens[0].camera.pos[2];
			bullet.velocity = new vec3(0,0,0);
		}
		else{
			var loc = SpaceToTile(bullet.position[0], bullet.position[2]);
			for (var j = 0; j < enemies.length; j++){
				var enemy = screens[0].GetObject(enemies[j]);
				var locB = SpaceToTile(enemy.position[0]+enemy.parent.position[0], enemy.position[2]+enemy.parent.position[2]);
				if (loc[0] == locB[0] && loc[1] == locB[1]) {
					bullet.position[0] = screens[0].camera.pos[0];
					bullet.position[1] = screens[0].camera.pos[1]-2.0;
					bullet.position[2] = screens[0].camera.pos[2];
					bullet.velocity = new vec3(0,0,0);
					
					enemies.splice(j, 1);
					enemy.position[1] -= 10;
					//if (audio_explode.currentTime > 0.5){
						//audio_explode.currentTime = 0;
					//}
					//audio_explode.play();
				}
			}
		}
	}
	
}

var reloadTimer = false;
function leftClickCall(callBack) {
	if (bullets.length == 0) return false;
	shootSound();
	
	var bulletID = bullets.shift();
	var bullet = screens[0].GetObject(bulletID);
	
	bullet.position[0] = screens[0].camera.pos[0];
	bullet.position[1] = screens[0].camera.pos[1]-.25;
	bullet.position[2] = screens[0].camera.pos[2];
	bullet.velocity = new vec3()
	
	bullet.velocity[0] = current_TDIR[0];
	bullet.velocity[1] = current_TDIR[1];
	bullet.velocity[2] = current_TDIR[2];
	
	spentBullets.push(bulletID);
}

function reload(){
	reloadTimer = new Date();
	while (bullets.length > 0){
		spentBullets.push(bullets.pop());
	}
	console.log("Reloading...");
}

function reloadBullets(){
	while (spentBullets.length > 0) {
		var bulletID = spentBullets.shift();
		var bullet = screens[0].GetObject(bulletID);
		bullet.position[0] = screens[0].camera.pos[0];
		bullet.position[1] = screens[0].camera.pos[1]-25;
		bullet.position[2] = screens[0].camera.pos[2];
		bullet.velocity = new vec3()
		
		bullets.push(bulletID);
	}
	reloadTimer = false;
	console.log("Reloaded!");
}

function shootSound(){
	//if (audio_shoot.currentTime > 0.5){
	//	audio_shoot.currentTime = 0;
	//}
	//audio_shoot.play();
}

function leftClickUp(callBack) {
	//audio_shoot.pause();
	//audio_shoot.load();
}
