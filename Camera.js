function SceneCam(gl, program, p, cU, cV, cD)
{
    this.pos = p;
    this.camU = cU;
    this.camV = cV;
    this.camDir=cD;
    this.vmat = new Matrix4();
    this.rmat = new Matrix4();

    this.rotscale = 1.0;
    this.mscale = 10;
    this.velocity = vec3(0,0,0);
    
    this.xmat = new Matrix4();
    this.ymat = new Matrix4();
    
    this.rotvel = 0;
    this.camPosLoc = gl.getUniformLocation(program, "camPos");
    this.camULoc = gl.getUniformLocation(program, "camU");
    this.camVLoc = gl.getUniformLocation(program, "camV");
    this.camDirLoc = gl.getUniformLocation(program, "camDir");
    this.odate = new Date();
    this.codate;
    
    this.xang = 0;
    this.yang = 0;
    this.move = -1;
    
    this.RotateVel = CameraRotateVel;
    this.Rotate = CameraRotate;
    this.Update = UpdateCamera;
    this.ReInit = CameraReInit;
    this.Move = CamMove;
}

function CameraReInit(program)
{
    this.camPosLoc = gl.getUniformLocation(program, "camPos");
    this.camULoc = gl.getUniformLocation(program, "camU");
    this.camVLoc = gl.getUniformLocation(program, "camV");
    this.camDirLoc = gl.getUniformLocation(program, "camDir");
}

function CameraRotateVel(v)
{
    this.rotvel = v;
}

function CameraRotate(x, y)
{
    var trotscale = this.rotscale/(fps);


    this.xang += trotscale*x;
    this.yang += trotscale*y;
	if (this.yang > 80) this.yang = 80;
	if (this.yang < -80) this.yang = -80;
}

function vec3Add(a, b)
{
    var temp = new vec3();
    temp[0] = a[0] + b[0];
    temp[1] = a[1] + b[1];
    temp[2] = a[2] + b[2];
    return temp;
}

function CamMove(i)
{
	if (this.move === -1) this.move = i;
	else if (this.move === 0){
		if (i === 1) i = -1;
		else if (i === 2) i = 7;
		else if (i === 3) i = 4;
	}
	else if (this.move === 1){
		if (i === 0) i = -1;
		else if (i === 2) i = 6;
		else if (i === 3) i = 5;
	}
	else if (this.move === 2){
		if (i === 3) i = -1;
		else if (i === 0) i = 7;
		else if (i === 1) i = 6;
	}
	else if (this.move === 3){
		if (i === 2) i = -1;
		else if (i === 0) i = 4;
		else if (i === 1) i = 5;
	}
	this.move = i;
	//console.log(i);
}

function TranslateCam(i, tU, tV, tDir)
{
    var x = tDir[0];
    var y = tDir[1];
    var z = tDir[2];
    
    if(i == 0)
        return normalize(tDir);
    else if(i == 1)
        return normalize(vec3(-1.0*tDir[0], -1.0*y, -1.0*tDir[2]));
    else if(i == 2)
         return normalize(negate(tU));
    else if(i == 3)
        return normalize(tU);
	
	
	else if(i == 4){
		var temp = new vec3();
		temp[0] = x + tU[0];
		temp[1] = y + tU[1];
		temp[2] = z + tU[2];
		return normalize(temp);
	}
	else if(i == 5){
		var temp = new vec3();
		temp[0] = -1.0*x + tU[0];
		temp[1] = -1.0*y + tU[1];
		temp[2] = -1.0*z + tU[2];
		return normalize(temp);
	}
	else if(i == 6){
		var temp = new vec3();
		temp[0] = -1.0*x + (tU[0])*-1.0;
		temp[1] = -1.0*y + (tU[1])*-1.0;
		temp[2] = -1.0*z + (tU[2])*-1.0;
		return normalize(temp);
	}
	else if(i == 7){
		var temp = new vec3();
		temp[0] = x + (tU[0])*-1.0;
		temp[1] = y + (tU[1])*-1.0;
		temp[2] = z + (tU[2])*-1.0;
		return normalize(temp);
	}
    return vec3(0,0,0);
}

function UpdateCamera(gl)
{
	var tempPos = new vec3(this.pos[0], this.pos[1], this.pos[2]);
    this.codate = new Date();
    //cInterp = (this.codate.getTime() - this.odate.getTime())/1000;
    this.odate = this.codate;
    //this.rmat.rotate(this.rotvel*cInterp, 0, 1, 0);
    var xmat = new Matrix4();
    var ymat = new Matrix4();
    
    
    xmat.rotate(this.xang, 0, 1, 0);
    
    var tU = Matrix4Vec3Mult(xmat, this.camU);
    var tDir = Matrix4Vec3Mult(xmat, this.camDir);
    ymat.rotate(this.yang, tU[0], tU[1], tU[2]);
    var tV = Matrix4Vec3Mult(ymat, this.camV);
    tDir = Matrix4Vec3Mult(ymat, tDir);
    
    /*console.log("tU"+tU);
    console.log("tDir"+tDir);
    console.log("tV"+tV);*/
    var tpos = TranslateCam(this.move, tU, tV, tDir);
	
	tempPos[0] += tpos[0]*120/fps;
	tempPos[1] += tpos[1]*120/fps;
	tempPos[2] += tpos[2]*120/fps;
    
    
	//console.log(CollisionMovement(this.pos[0], this.pos[2]));
	//tempPos = CollisionMovement(tempPos, this.pos, this.mscale);
	this.pos[0] = tempPos[0];
	this.pos[1] = tempPos[1];
	this.pos[2] = tempPos[2];
	
    gl.uniform3fv(this.camPosLoc, this.pos);
    gl.uniform3fv(this.camULoc, tU);
    gl.uniform3fv(this.camVLoc, tV);
    gl.uniform3fv(this.camDirLoc, tDir);
    return tDir;
}