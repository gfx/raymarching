var canvas;

//array of objects
var screens = [];
//global set of all points and colors

var objType = {
    SPHERE : 0,
    BOX : 1,
    CYLINDER : 2,
    FLOOR : 3,
    LEGO : 4,
};

var opType = {
    CHILD : -1,
    SUB : 0,
    INT : 1,
    UN : 2,
};

var mapscale = 3.0;
var _mapthreshold = 0.1;

var xAxis = 0;
var yAxis = 1;
var zAxis = 2;

var frameTime = 1;

var fpsSamNum = 10;
var sdate = new Date();
var tdate = new Date();
var edate = 0;

var avgFrameTime = -1.0;

var orig_date = false;

var frames = 1;

var fps = 60.0;

var theta = Math.PI/4;
var phi = Math.PI/3;
var projDist = 1.0;
var camScale = 1.0;

var camRotSpeed = 20;

var axis = 2;

var programs = [];
var numPrograms = 0;

window.onload = function init()
{   
    canvas = document.getElementById( "gl-canvas" );
    this.bullets = [];
    
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0, 0, 0, 1.0 );
    
    gl.enable(gl.DEPTH_TEST);

   var sceneString = "rdist SceneDist(vec3 p, vec3 u){return rdist(1.123, -1, vec3(1.0, 1.0, 1.0), vec4(0, 0, 0, 0), 0.0);}";
    
    var program = initShaders( gl, "vertex-shader", "fragment-shader" , sceneString, 1);
    gl.useProgram( program );
    
    //Initialize camera axis
    var camX = new vec3(camScale,0.0,0.0);
    var camY = new vec3(0.0,camScale*(canvas.height/canvas.width),0.0);
    var camZ = new vec3(0.0,0.0,projDist);
    
    //Create quad for the screen
    addScreen(new coloredQuad(gl,program,new vec3(0,0,0),1.0, camX, camY, camZ));
    
    var modHeaderStr = screens[0].GetModStr();
    
    InitObjects(program);
    
    program = initShaders( gl, "vertex-shader", "fragment-shader" , modHeaderStr+screens[0].SceneString(), screens[0].GetNumObjects()); 
    gl.useProgram( program );
    screens[0].ReInit(program);
    
    //screens[0].objects[2].AddDependency(opType.INT, 3);
    
    console.log(modHeaderStr+screens[0].SceneString());
    
    document.getElementById( "slowProg" ).onclick = function () 
    {
        console.log(screens[0].SlowSceneString());
        program = initShaders( gl, "vertex-shader", "fragment-shader" , modHeaderStr+screens[0].SlowSceneString(), screens[0].GetNumObjects()); 
        gl.useProgram( program );
        screens[0].ReInit(program);
    } 
    
    document.getElementById( "accelProg" ).onclick = function () 
    {
        program = initShaders( gl, "vertex-shader", "fragment-shader" , modHeaderStr+screens[0].SceneString(), screens[0].GetNumObjects()); 
        gl.useProgram( program );
        screens[0].ReInit(program);
    } 
    
    HandleEvents(gl, program);

    
        
    render();
}

function InitObjects(program)
{
    //TODO make Builder class to make scene creation easier.
    screens[0].AddObject(gl, program, objType.SPHERE, 3, vec3( 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), 2000.0 );
    
    //Add Floor
    screens[0].AddObject(gl, program, objType.FLOOR, 1, vec3( 0.0, -3.0, 0.0), vec4(.1, .1, .1, 1.0), 1.0, vec3(20.0, 2.0, 20.0));   
    screens[0].AddDependency(opType.CHILD, 0, 1);
    screens[0].GetObject(1).reflective = .5;

    //Add box with intersection of spheres removed
    screens[0].AddObject(gl, program, objType.SPHERE, 2,vec3( 0.0, 75.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), 150.0);
    screens[0].AddObject(gl, program, objType.BOX, 1,vec3( 0.0, 0.0, 0.0), vec4(1.0, 0.1, 0.1, 1.0), 60.0, vec3(1.0, 2.0, 1.0) );
    screens[0].AddObject(gl, program, objType.SPHERE, 0, vec3( 0.0, -0.5, 0.0), vec4(1.0, 1.0, 1.0, 1.0), 80.0);
    screens[0].AddObject(gl, program, objType.SPHERE, 0, vec3( 0.0, 0.5, 0.0), vec4(1.0, 1.0, 1.0, 1.0), 80.0);
    
    screens[0].AddDependency(opType.CHILD, 0, 2);
    screens[0].AddDependency(opType.CHILD, 2, 3);
    screens[0].AddDependency(opType.SUB, 3, 4);
    screens[0].AddDependency(opType.INT, 4, 5);

    //Add dome
    /*id = screens[0].AddObject(gl, program, objType.BOX, 1,vec3( 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), 1000.0, vec3(1.5, 1.0, 1.5));
    screens[0].AddObject(gl, program, objType.BOX, 1,vec3( 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), 500.0, vec3(1.5, 2.0, 1.5) );
    screens[0].AddDependency(opType.CHILD, 0, id);
    screens[0].AddDependency(opType.SUB, id, id + 1);*/
    //screens[0].GetObject(id + 1).SetAngVel(.5, 1);

    id = screens[0].AddObject(gl, program, objType.SPHERE, 1, vec3( 0.0, 300.0, 0.0), vec4(0.0, 0.7, 1.0, 1.0), 100.0);
    screens[0].AddDependency(opType.CHILD, 0, id);
    screens[0].GetObject(id).SetAngVel(Math.random()*.2, 0);
    screens[0].GetObject(id).reflective = .9999999;

    id = screens[0].AddObject(gl, program, objType.CYLINDER, 1, vec3( -250.0, 200.0, 0.0), vec4(0.0, 0.7, 1.0, 1.0), 1.0, vec3(25.0, 300.0, 1.0));
    screens[0].AddDependency(opType.CHILD, 0, id);

    id = screens[0].AddObject(gl, program, objType.CYLINDER, 1, vec3( 250.0, 200.0, 0.0), vec4(0.0, 0.7, 1.0, 1.0), 1.0, vec3(25.0, 300.0, 1.0));
    screens[0].AddDependency(opType.CHILD, 0, id);


    scale = 25.0;
    id = screens[0].AddObject(gl, program, objType.SPHERE, 2,vec3( 0.0, scale/2, 0.0), vec4(1.0, 1.0, 1.0, 1.0), 8.0 * scale);
    screens[0].AddDependency(opType.CHILD, 0, id);
    for(var i = 0; i < 5; i++)
    {
        width = 5.0 - i * .5;
        id2 = screens[0].AddObject(gl, program, objType.BOX, 1,vec3( 0.0, i*scale/2, 0.0), vec4(1.0, 1.0, 1.0, 1.0), scale, vec3(width, .5, width));
        screens[0].AddDependency(opType.CHILD, id, id2);
    }

    //LoadLegoModel(modelLego1, 1.0, id, program);
    //obj = CreateModelJson(screens[0].objHandler, id, "LegoSuzanne");
    //console.log(JSON.stringify(obj, null, 2));
    //console.log("LEGO DICTIONARY")
    //console.log(modelLego1["3,5,5"]["size"])

    /*id2 = screens[0].AddObject(gl, program, objType.LEGO, 1, vec3( 0.0, 20.0, -300.0), vec4(0.05, 0.05, 0.05, 1.0), 1.0, vec3(modelLego1["3,5,5"]["size"][0], modelLego1["3,5,5"]["size"][1], modelLego1["3,5,5"]["size"][2]));
    screens[0].AddDependency(opType.CHILD, id, id2);
    screens[0].GetObject(id2).reflective = .01;*/

    id = screens[0].AddObject(gl, program, objType.SPHERE, 2, vec3(  0.0, 30.0, -300.0 ), vec4(1.0, 1.0, 1.0, 1.0), 50.0);
    screens[0].AddDependency(opType.CHILD, 0, id);
    screens[0].GetObject(id).SetAngVel(30.0 * .5/fps, 1);

    id2 = screens[0].AddObject(gl, program, 100, 1, vec3(  0.0, 0.0, 0.0 ), vec4(1.0, 1.0, 1.0, 1.0), 1.0);
    screens[0].AddDependency(opType.CHILD, id, id2);
}

function LoadLegoModel(model, scale, pid, program)
{
    var height = scale*9.6/3.0/2.0;
    var width = scale*8.0/2.0;

    var i = 0;
    var id2 =null;
    for(var key in model)
    {
        item = model[key]


        if(item["top_exposed"] == true)
            id2 = screens[0].AddObject(gl, program, objType.LEGO, 1, vec3( scale * item["coor"][0], scale * item["coor"][2], scale * item["coor"][1]), RandomColor(), scale, vec3(item["size"][0], item["size"][1], item["size"][2]));
        else
            id2 = screens[0].AddObject(gl, program, objType.BOX, 1, vec3( scale * item["coor"][0], scale * item["coor"][2], scale * item["coor"][1]), RandomColor(), scale, vec3(item["size"][0]*width, item["size"][2]*height, item["size"][1]*width));
        
        screens[0].AddDependency(opType.CHILD, pid, id2);

        if(i >= 20)
            break;
        //i++;
    }
}

function GenerateLegoBrick(scale, zScale, program)
{
    height = scale*9.0*(zScale/3)/2.0;
    width = scale*8.0/2.0;
    studHeight = scale*1.8/2.0;
    idBlock = screens[0].AddObject(gl, program, objType.BOX, 1, vec3( 0.0, 0.0, 0.0), vec4(0.0, 0.7, 1.0, 1.0), scale, vec3(width, height, width));
    idStud = screens[0].AddObject(gl, program, objType.CYLINDER, 1, vec3( 0.0, height + studHeight, 0.0), vec4(0.0, 0.7, 1.0, 1.0), 1.0, vec3(scale*2.4, studHeight, 1.0));
    screens[0].AddDependency(opType.UN, idBlock, idStud);
    return idBlock;
}

function GenerateNbyMLegoBrick(pid, n, m, scale, zScale, reflectance, color, program)
{
    height = scale*9.0*(zScale/3)/2.0;
    width = scale*8.0/2.0;
    studHeight = scale*1.8/2.0;
    studWidth = scale*2.4;
    idBlock = screens[0].AddObject(gl, program, objType.BOX, 1, vec3( 0.0, 0.0, 0.0), color, 1.0, vec3(n*width, height, m*width));
    screens[0].GetObject(idBlock).reflective = reflectance;
    screens[0].AddDependency(opType.CHILD, pid, idBlock);
    for(var i =0; i < n; i++){
        for(var j = 0; j < m; j++)
        {
            idStud = screens[0].AddObject(gl, program, objType.CYLINDER, 1, vec3( -1*(n-1)*width + i * width * 2.0, height + studHeight, -1*(m-1)*width + j * width * 2.0), color, 1.0, vec3(studWidth, studHeight, 1.0));
            screens[0].AddDependency(opType.CHILD, pid, idStud);
            screens[0].GetObject(idStud).reflective = reflectance;
        }
    }
    return idBlock;
}

function RandomColor()
{
    return vec4(Math.random(), Math.random(), Math.random(), 1.0);
}

function addScreen(obj)
{
    screens.push(obj);
}

function HandleFps()
{
    if(frames == fpsSamNum)
    {
        edate = new Date();
        avgFrameTime=((edate.getTime() - sdate.getTime())/1000/fpsSamNum);
        fps = 1/avgFrameTime;
        document.getElementById('fps').innerHTML = 'FPS: ' + fps;
        frames = 1;
        sdate = edate;
    }
    else
        frames += 1;
}

function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    HandleFps();
    for(var i = 0; i < screens.length; i++)
    {
        screens[i].draw(gl);
    }
    requestAnimationFrame( render );
}

function HandleEvents(gl, program)
{
	var controlPointer = false;
	canvas.onclick = function () {
		if (!controlPointer){
			canvas.requestPointerLock();
			
			if (! orig_date) {
				orig_date = new Date();
				//audio_bgm.play();
			}
			
			var havePointerLock = "pointerLockElement" in document;
			document.addEventListener("pointerlockchange", mouseCaptureChange, false);
			document.addEventListener("mousemove", mouseMoveCall, false);
			document.addEventListener("keydown", keyDownCall, false);
			document.addEventListener("keyup", keyUpCall, false);
			document.addEventListener("mousedown", leftClickCall, false);
		}
	}
	
	function mouseCaptureChange(callBack) {
		if (controlPointer ){
			document.removeEventListener("mousemove", mouseMoveCall, false);
			document.removeEventListener("keydown", keyDownCall, false);
			document.removeEventListener("keyup", keyUpCall, false);
			document.removeEventListener("mousedown", leftClickCall, false);
			wPressed = false,
			aPressed = false,
			sPressed = false,
			dPressed = false;
			screens[0].camera.Move(-1);
			controlPointer = false;
		}
		else {
			controlPointer = true;
		}
	}
	
	function mouseMoveCall(callBack) {
		var movementX = callBack.movementX;
		movementY = callBack.movementY;
		screens[0].camera.Rotate(movementX, movementY);
    }
    
	var wPressed = false,
		aPressed = false,
		sPressed = false,
		dPressed = false;
	function keyDownCall(callBack) {
		character = String.fromCharCode(callBack.keyCode);
		//console.log(character);
		if (character === 'W') wPressed = true;
		if (character === 'S') sPressed = true;
		if (character === 'A') aPressed = true;
		if (character === 'D') dPressed = true;
		if (character === 'R') reload();
		
		if (wPressed && dPressed) screens[0].camera.Move(4);
		else if (sPressed && dPressed) screens[0].camera.Move(5);
		else if (sPressed && aPressed) screens[0].camera.Move(6);
		else if (wPressed && aPressed) screens[0].camera.Move(7);
		
		else if (wPressed) screens[0].camera.Move(0);
		else if (sPressed) screens[0].camera.Move(1);
		else if (aPressed) screens[0].camera.Move(2);
		else if (dPressed) screens[0].camera.Move(3);
	}
    
	function keyUpCall(callBack) {
		character = String.fromCharCode(callBack.keyCode);
		//
		//console.log(character);
		if (character === 'W') wPressed = false;
		if (character === 'S') sPressed = false;
		if (character === 'A') aPressed = false;
		if (character === 'D') dPressed = false;
		
		if (! (wPressed || aPressed || sPressed || dPressed)) screens[0].camera.Move(-1);
		else {
			if (wPressed && dPressed) screens[0].camera.Move(4);
			else if (sPressed && dPressed) screens[0].camera.Move(5);
			else if (sPressed && aPressed) screens[0].camera.Move(6);
			else if (wPressed && aPressed) screens[0].camera.Move(7);
			
			else if (wPressed) screens[0].camera.Move(0);
			else if (sPressed) screens[0].camera.Move(1);
			else if (aPressed) screens[0].camera.Move(2);
			else if (dPressed) screens[0].camera.Move(3);		
		}
	}
    
    //Some button functionality for debug
    /*document.getElementById( "leftCamRot" ).onclick = function () {
        screens[0].camera.RotateVel(-camRotSpeed);
    };
    document.getElementById( "rightCamRot" ).onclick = function () {
        screens[0].camera.RotateVel(camRotSpeed);
    };
    document.getElementById( "stopCamRot" ).onclick = function () {
        screens[0].camera.RotateVel(0);
    };
    document.getElementById( "addObj" ).onclick = function () {
        var f = document.getElementById("ObjectForm");
        var pid = parseInt(f.elements[0].value);        
    };
    
    document.getElementById( "accelProg" ).onclick = function () {
        program = initShaders( gl, "vertex-shader", "fragment-shader" , modHeaderStr+screens[0].SceneString(), screens[0].GetNumObjects()); 
        gl.useProgram( program );
        screens[0].ReInit(program);
    } 
    
    document.getElementById( "slowProg" ).onclick = function () {
        console.log(screens[0].SlowSceneString());
        program = initShaders( gl, "vertex-shader", "fragment-shader" , modHeaderStr+screens[0].SlowSceneString(), screens[0].GetNumObjects()); 
        gl.useProgram( program );
        screens[0].ReInit(program);
    } 
    
    document.getElementById( "AddFormObj" ).onclick = function () {
        var f = document.getElementById("ObjectForm");
        var pid = parseInt(f.elements[0].value);
        var type = parseInt(f.elements[1].value);
        var root = parseInt(f.elements[2].value);
        
        var tposition = f.elements[3].value;  
        var position = ParseVecString(tposition);
        
        var tcolor = f.elements[4].value;  
        var color = ParseVecString(tcolor);
        
        var scale= parseFloat(f.elements[5].value);
        
        var trot = f.elements[6].value;  
        var rotation = ParseVecString(trot);
        
        var texvec = f.elements[7].value; 
        var exvec = ParseVecString(texvec);
            console.log("exvec " + exvec);
            
         var id = screens[0].AddObject(gl, program, type, root, position, vec4(color, 1.0), scale, this.modHandle,exvec);
         screens[0].AddDependency(opType.CHILD, pid, id);
         screens[0].objHandler.SetObjRot(id, rotation[0], rotation[1], rotation[2]);
        program = initShaders( gl, "vertex-shader", "fragment-shader" , modHeaderStr+screens[0].SceneString(), screens[0].GetNumObjects()); 
        gl.useProgram( program );
        screens[0].ReInit(program);
    };*/
}