//TODO Rewrite all of this... It's disgusting

STRDISTDEFAULT = "rdist(10000.0, -1, vec3(1.0, 1.0, 1.0), vec4(0,0,0,0), 0.0)";

function ObjectHandler()
{
    this.numObjects = 0;
    this.numDObjects = 0;
    
    this.objects = [];
    
    this.modHandler;
    
    this.iteratorIndex = -1; 

    this.SetModHandle = SetModelHandler;
    this.AddObject = AddNewObject;
    this.AddDependency = CreateObjectDependency;
    this.ReInit = ObjHandReInitShader;
    this.SceneString = GenerateSceneString;
    this.SlowSceneString = GenerateRawSceneString;
    this.UpdateShader = UpdateObjectShaders;
    this.GetNumObjects = GetNumObjects;
    this.GetObject = GetObject;
    this.GenJsonStr = GetJsonObj;
    this.SetObjRot = SetSubObjRotation;
}

function GetObject(id)
{
    return this.objects[id];
}

function GetNumObjects()
{
    return this.numObjects;
}

function SetModelHandler(modHandle)
{
    this.modHandler = modHandle;
}

function AddNewObject(gl, program, type, root, pos, color, scale, extraVec = vec3(1.0, 1.0, 1.0))
{
    this.objects.push(new SceneObject(gl, program, this.numObjects, type, root, pos, color, scale, this.modHandler, extraVec));
    this.numObjects += 1;
    return this.numObjects - 1;
}

function CreateObjectDependency(op, pid, cid)
{
    this.objects[pid].AddDependency(op, cid);
}

function ObjHandReInitShader(program)
{
    for(var i = 0; i < this.numObjects; i++)
    {
        this.objects[i].ReinitShader(program);
    }
}

function UpdateObjectShaders()
{
    for(var i = 0; i < this.numObjects; i++)
    {
        this.objects[i].UpdateShader(gl);
    }    
}

function SetSubObjRotation(id, tx, ty, tz)
{
    this.objects[id].SetRot(tx, ty, tz);
}

function GenerateSceneString()
{
    var s = "";
    var sceneDistCalls = "";
    var rootObjIndex = -1;
    for(var i =  0; i < this.numObjects; i++)
    {
        if (this.objects[i].root == 3)
        {
            rootObjIndex = this.objects[i].id;
            break;
        }
    }
    s += GenerateShaderObjFunctions(this, rootObjIndex);
    
    s += "rdist SceneDist(vec3 p, vec3 u)\n{\n";
    s += "\treturn normMult("+this.objects[rootObjIndex].GetString()+", objNormMats[0]);\n}\n";
    return s;
}

function GetJsonObj(i, name)
{
    var str = "var " + name + " = {\n"
            +'"alias" : "' +name+'",\n'
            +'\t"fdef" : "rdist ' +name+ '(vec3 p, vec3 u)\\n"\n';
     var funcstr = GenerateSceneFunction(this, i, 1);
     var astr = funcstr.split("\n");
     for(var j = 1; j < astr.length-2; j++)
     {
        str += '\t+"' + astr[j] + '\\n"\n';
     }
     str += "}";
     return str;
}

function GenerateSceneFunction(scene, i, verbose)
{
        return "rdist Scene" +  scene.objects[i].id + "(vec3 p, vec3 u)\n{\n" + GenerateSubSceneString(scene, scene.objects[i].dependencies, scene.objects[i].numDeps, verbose) + "}\n\n";
}

function GenerateAccelFunction(scene, i)
{
    return "rdist Accel" + scene.objects[i].id + "(vec3 p, vec3 u)\n"+
    "{\n\tfloat t = dot(objPositions["+ i + "] -p,u);\n"+
    "\tvec3 q = p+t*u;\n"+
    "\tfloat d2 = "+scene.objects[i].scale+"*"+scene.objects[i].scale+"- len2(q-objPositions["+ i + "] );\n"+
    "\tif(d2 <= 0.0)\n"+
    "\t\treturn " + STRDISTDEFAULT + ";\n"+
    "\td2 = sqrt(d2);\n"+
    "\tif(t+d2 < 0.0)\n"+
    "\t\treturn " + STRDISTDEFAULT + ";\n"+
    "\tif(t-d2 < 0.0)\n"+
    "\t\treturn Scene" + scene.objects[i].id + "(vec3(objMats["+scene.objects[i].id+"]*vec4(p, 1.0)),vec3(objMats["+scene.objects[i].id+"]*vec4(u, 0.0)));\n"+
    "\treturn rdist(t - d2 + threshold, -1, vec3(1.0,1.0,1.0), vec4(0, 0, 0, 0), 0.0);\n}\n\n";
}

function GenerateRawAccelFunction(scene, i)
{
    return  "rdist Accel" + scene.objects[i].id + "(vec3 p, vec3 u)\n{\n"+
    "\treturn Scene" + scene.objects[i].id + "(vec3(objMats["+scene.objects[i].id+"]*vec4(p, 1.0)),vec3(objMats["+scene.objects[i].id+"]*vec4(u, 0.0)));\n}\n";
}

function GenerateShaderObjFunctions(scene, i, raw = 0)
{
    var tempString = "";
    for(var j = 0; j < scene.objects[i].numDeps; j++)
    {
        tind = scene.objects[i].dependencies[j].id
        //if(scene.objects[tind].root == 2)
        tempString += GenerateShaderObjFunctions(scene, tind, raw);
    }
    if(scene.objects[i].root == 2 || scene.objects[i].root == 3)
    {
        tempString += GenerateSceneFunction(scene, i, 0);
        if(raw == 0)
            tempString += GenerateAccelFunction(scene, i);
        else
            tempString += GenerateRawAccelFunction(scene, i);
    }
    return tempString;
}

function GenerateRawSceneString()
{
    var s = "";
    var sceneDistCalls = "";
    var rootObjIndex = -1;
    for(var i =  0; i < this.numObjects; i++)
    {
        if (this.objects[i].root == 3)
        {
            rootObjIndex = this.objects[i].id;
            break;
        }
    }
    s += GenerateShaderObjFunctions(this, rootObjIndex, 1);
    
    s += "rdist SceneDist(vec3 p, vec3 u)\n{\n";
    s += "\treturn normMult("+this.objects[rootObjIndex].GetString()+", objNormMats[0]);\n}\n";
    return s;
}

function GenerateSubSceneString(scene, objList, numObjs, verbose)
{
    var numV = 0;
    var s = "\trdist rm = " + STRDISTDEFAULT + ";\n";
    for(var j =  0; j < numObjs; j++)
    {
        if(scene.objects[objList[j].id].root != 0)
        {
            s += "\trm" +  " = rmin(rm, " + GenerateSceneStringHelper(scene, objList[j].id, verbose) + ");\n";
            numV++;
        }
    }
    return s+"\treturn rm;\n";
}

function GenerateSceneStringHelper(scene, i, verbose)
{
    if(verbose == 0)
    {
        var currString = "normMult( " + scene.objects[i].GetString() + ", objNormMats["+scene.objects[i].id+"])";
        if(scene.objects[i].numDeps == 0 || scene.objects[i].root == 2)
            return currString;

        for(var j = 0; j < scene.objects[i].dependencies.length; j++)
        {
            var top = scene.objects[i].dependencies[j];
            switch(top.op){
                case 0:
                    currString = "rmax(" + currString + ", rneg( "  + GenerateSceneStringHelper(scene, top.id, verbose)+"))";
                    break;
                case 1:
                    currString = "rmax(" + currString + ", " + GenerateSceneStringHelper(scene, top.id, verbose)+")";
                    break;
                case 2:
                    currString = "rmin(" + currString + ", " + GenerateSceneStringHelper(scene, top.id, verbose)+")";
                    break;
                 default:
                    return "INVALID OPERATION?!!!!?";
                 }
             }
             return currString
    }
    else
    {
        if(scene.objects[i].numDeps == 0 || scene.objects[i].root == 2)
            return scene.objects[i].GetVerboseString();
        //var top = scene.objects[i].dependencies[0];
        var currString = scene.objects[i].GetVerboseString();
        for(var j = 0; j < scene.objects[i].dependencies.length; j++)
        {
            var top = scene.objects[i].dependencies[j];
            switch(top.op){
                case 0:
                    currString = "rmax(" + currString + ", rneg(" +GenerateSceneStringHelper(scene, top.id, verbose)+"))";
                    break;
                case 1:
                    currString = "rmax(" + currString + ", " +GenerateSceneStringHelper(scene, top.id, verbose)+")";
                    break;
                case 2:
                    currString = "rmin(" + currString + ", " +GenerateSceneStringHelper(scene, top.id, verbose)+")";
                    break;
                 default:
                    return "INVALID OPERATION?!!!!?";
                 }
             }
             return currString  
    }
}

function CreateModelJson(scene, containerId, modelName)
{
    funcString = "rdist " +  modelName + "(vec3 p, vec3 u)\n{\n" + GenerateSubSceneString(scene, scene.objects[containerId].dependencies, scene.objects[containerId].numDeps, 1) + "}\n\n";
    return { alias: modelName, fdef: funcString };
}