function ModelHandler()
{
    this.num_models = 0;
    this.model_names = [];
    this.models = [];
    
    this.AddModel = AddModel;
    this.GetString = CreateModCallString;
    this.FuncDefs = GetModDefs;
}

function AddModel(obj)
{
    this.models.push(new Model(this.num_models, obj.fdef, obj.alias));
    this.model_names.push(obj.alias);
    this.num_models++;
    return this.num_models-1+100;
}

function CreateModCallString(modid, id )
{
    // return primitive distance function
    modid -= 100;
    return this.models[modid].func_name+"(vec3(objMats["+id+"]*vec4(p, 1.0)),vec3(objMats["+id+"]*vec4(u, 0.0)))";
}

function GetModDefs()
{
    str = "\n";
    for(var i = 0; i < this.models.length; i++)
    {
        str += this.models[i].str_func + "\n\n";
    }
    return str;
}