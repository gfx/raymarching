function coloredQuad(gl,program,pos,scale, camOriU, camOriV, camOriZ)
{

    this.camera = new SceneCam(gl,program,vec3(0.0,85.0,-700.0), camOriU, camOriV, camOriZ);
    this.lightPos = vec3(0.0, 200.0, -650.0);
    this.scale = scale;
    this.pos = pos;
    this.theta = [0.0, 0.0, 0.0];
    
    this.objHandler = new ObjectHandler();
    this.modHandler = new ModelHandler();
    
    this.modHandler.AddModel(LegoSphere);
    //this.modHandler.AddModel(LegoSuzanne);
    
    this.accel = 0;
    
    this.matLoc = gl.getUniformLocation(program, "mat");
    this.accelLoc = gl.getUniformLocation(program,"accel");
    this.lightPosLoc = gl.getUniformLocation(program, "lightPos");
    
    
    
    
    
    this.corners = [
        vec3( -1.0, -1.0, 0.0),
        vec3( -1.0,  1.0, 0.0),
        vec3(  1.0,  1.0, 0.0),
        vec3(  1.0, -1.0, 0.0),
    ];
    
    this.points = [];
    this.colors = [];
    
    this.us = [];
    this.vs = [];
    
    addQuad( this, 1, 0, 3, 2 );
    
    this.vertexBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, this.vertexBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(this.points), gl.STATIC_DRAW );
    
    this.colorBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, this.colorBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(this.colors), gl.STATIC_DRAW ); 
    
    this.uBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, this.uBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(this.us), gl.STATIC_DRAW );
    
    this.vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, this.vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(this.vs), gl.STATIC_DRAW );
    
    this.vColor = gl.getAttribLocation( program, "vColor" );
    this.vPosition = gl.getAttribLocation( program, "vPosition" );
    this.vU = gl.getAttribLocation( program, "vU");
    this.vV = gl.getAttribLocation( program, "vV");

    this.draw = drawColoredQuad;
    this.AddObject = addRObject;
    this.AddDependency = AddObjectDependency;
    this.SceneString = GetSceneString;
    this.SlowSceneString = GetRawSceneString;
    this.ReInit = ReInitialize;
    this.GenerateRandAccel = GenerateRandContainer;
    this.GetNumObjects = GetHandleNumObjects;
    this.GetObject = GetHandleObject;
    this.GetModStr = GetModelString;
}

function GetModelString()
{
    return this.modHandler.FuncDefs();
}

function GetHandleNumObjects()
{
    return this.objHandler.GetNumObjects();
}

function GetHandleObject(id)
{
    return this.objHandler.GetObject(id);
}

function addRObject(gl, program, type, root, pos, color, scale, extraVec = vec3(1.0, 1.0, 1.0))
{
    this.objHandler.SetModHandle(this.modHandler);
    return this.objHandler.AddObject(gl, program, type, root, pos, color, scale, extraVec);
}

function AddObjectDependency(op, pid, cid)
{
    this.objHandler.AddDependency(op, pid, cid);
}

function GetSceneString()
{
    return this.objHandler.SceneString();
}

function GetRawSceneString()
{
    return this.objHandler.SlowSceneString();
}

function ReInitialize(program)
{
    this.matLoc = gl.getUniformLocation(program, "mat");
    this.lightPosLoc = gl.getUniformLocation(program, "lightPos");    
    this.camera.ReInit(program);
    
    this.vColor = gl.getAttribLocation( program, "vColor" );
    this.vPosition = gl.getAttribLocation( program, "vPosition" );
    this.vU = gl.getAttribLocation( program, "vU");
    this.vV = gl.getAttribLocation( program, "vV");
    
    this.objHandler.ReInit(program);
}

function addQuad(object, a, b, c, d)
{   
    var indices = [ a, b, c, a, c, d ];

    for ( var i = 0; i < indices.length; ++i ) {
        object.points.push( object.corners[indices[i]] );
        object.colors.push( vec4(0.0, 0.0, 0.0, 1.0) );
        object.us.push( object.corners[indices[i]][0] );
        object.vs.push( object.corners[indices[i]][1] );
    }   
}

function GenerateRandContainer(gl, program, numObjs, scale, range)
{
    pos = vec3(-range/2+range*Math.random(), 5*Math.random() + scale/2.0, -range/2+range*Math.random());
    s = .5*scale+.5*Math.random()*scale;
    oscale = .15*s;
    id = this.AddObject(gl, program, 0, 2, pos, vec4(0.0, 0.8, 1.0, 1.0), s);
    for(var i = 0; i < numObjs; i++)
    {
        console.log("making a new object!");
        x = -.5*s+s*Math.random();
        y = -.5*s+s*Math.random();
        z = -.5*s+s*Math.random();
        var cid = this.AddObject(gl, program, Math.floor(Math.random()+.5), 1, vec3(x, y, z), vec4(Math.random(), Math.random(), Math.random(), 1.0), 0.3*oscale+Math.random()*oscale*.5);
        
        this.AddDependency(-1, id, cid);
        this.GetObject(cid).SetAngVel(Math.random(), Math.floor(Math.random()*2.0));
    }
    this.AddDependency(-1, 0, id);
    this.GetObject(id).SetAngVel(Math.random() * .2, 1);
}

function Matrix4Vec3Mult(m, p)
{
    mat = m.elements;
    var result = new vec3();
    result[0] = p[0] * mat[0] + p[1] * mat[4] + p[2] * mat[ 8] + mat[11];
    result[1] = p[0] * mat[1] + p[1] * mat[5] + p[2] * mat[ 9] + mat[12];
    result[2] = p[0] * mat[2] + p[1] * mat[6] + p[2] * mat[10] + mat[13];
    return result;
}


function drawColoredQuad(gl)
{
   // this.theta[axis] = 0.5;
    var mat = new Matrix4();
    mat.translate(this.pos[0],this.pos[1],this.pos[2]);
    mat.scale(this.scale,this.scale,this.scale);
    mat.rotate(this.theta[2], 0, 0, 1);
    mat.rotate(this.theta[1], 0, 1, 0);
    mat.rotate(this.theta[0], 1, 0, 0);
    
    gl.uniformMatrix4fv(this.matLoc, false, mat.elements);
    
    current_TDIR = this.camera.Update(gl);

    this.objHandler.UpdateShader();
    
    gl.uniform1i(this.numObjLoc, this.numObjects);
    gl.uniform1i(this.accelLoc, this.accel);

    var mat2 = new Matrix4();
    //mat.translate(this.pos[0],this.pos[1],this.pos[2]);
    //mat.scale(this.scale,this.scale,this.scale);
    mat2.rotate(.001, 1, 0, 0);
    this.lightPos = Matrix4Vec3Mult(mat2, this.lightPos);
    gl.uniform3fv(this.lightPosLoc, this.lightPos);
    
    gl.bindBuffer( gl.ARRAY_BUFFER, this.colorBuffer );
    gl.vertexAttribPointer( this.vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( this.vColor );
    
    gl.bindBuffer( gl.ARRAY_BUFFER, this.vertexBuffer );
    gl.vertexAttribPointer( this.vPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( this.vPosition );
    
    gl.bindBuffer( gl.ARRAY_BUFFER, this.uBuffer );
    gl.vertexAttribPointer( this.vU, 1, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( this.vU );
    
    gl.bindBuffer( gl.ARRAY_BUFFER, this.vBuffer );
    gl.vertexAttribPointer( this.vV, 1, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( this.vV );
    
    gl.drawArrays( gl.TRIANGLES, 0, this.points.length );
}

