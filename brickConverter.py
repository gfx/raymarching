import json

null = None
false = False
true = True
brickDict = {};

height = 3.2
width = 8.0
studHeight = 1.8

def lAdd(l1, l2):
    return [l1[0]+l2[0],l1[1]+l2[1],l1[2]+l2[2]]

def lSub(l1, l2):
    return [l1[0]-l2[0],l1[1]-l2[1],l1[2]-l2[2]]

def lSDiv(l, s):
    return [l[0]/s,l[1]/s,l[2]/s]

print("Valid keys: ")

def FindBricks(dic):
    block = {}
    keys = list(dic.keys())
    for key in keys:
        if(brickDict[key]["parent_brick"] == "self" and brickDict[key]["draw"] == true):
            block[key] = brickDict[key]
    return block

def PrintDict(d, k):
    for key in d.keys():
        print(key + ": ", end = "")
        print(d[key][k])

def GetBrickCoorPos(lPos, bWidth, bHeight):
    return [bWidth * lPos[0] + bWidth/2, bWidth * lPos[1] + bWidth/2, bHeight * lPos[2] + bHeight/2]
    
def GetBlockCenter(lPos, lSize):
    lSA = [lSize[0] - 1, lSize[1] - 1, lSize[2] - 1]

    lPos = ScaleZ(lPos)
    lEnd = lAdd(lPos, lSA)

    startCoor = GetBrickCoorPos(lPos, width, height)
    endCoor = GetBrickCoorPos(lEnd, width, height)

    result = lSDiv(lAdd(startCoor, endCoor), 2)
    
    return result

def ScaleZ(l):
    return [l[0], l[1], 3*l[2]]

def FindOrigin(d):
    xMin = 10000
    yMin = 10000
    zMin = 10000
    xMax = 0
    yMax = 0
    zMax = 0
    
    keys = list(d.keys())
    for key in keys:
        item = ScaleZ(ParseKey(key))
        if(item[0] < xMin):
            xMin = item[0]
        if(item[1] < yMin):
            yMin = item[1]
        if(item[2] < zMin):
            zMin = item[2]

        lSize = d[key]["size"]
        lSA = [lSize[0] - 1, lSize[1] - 1, lSize[2] - 1]
        item = lAdd(item, lSA)
        
        if(item[0] > xMax):
            xMax = item[0]
        if(item[1] > yMax):
            yMax = item[1]
        if(item[2] > zMax):
            zMax = item[2]

    startCoor = GetBrickCoorPos([xMin, yMin, zMin], width, height)
    endCoor = GetBrickCoorPos([xMax, yMax, zMax], width, height)
        
    return lSDiv(lAdd(startCoor, endCoor), 2)


def ParseKey(s):
    l = s.split(",")
    for i in range(len(l)):
        l[i] = int(l[i])
    return l
       
blocks = FindBricks(brickDict)

print("Number of blocks: ", len(blocks))
#PrintDict(blocks, "size")

keys = list(blocks.keys())
for key in keys:
    lpKey = ParseKey(key)
    blocks[key]["coor"] = GetBlockCenter(lpKey, blocks[key]["size"])

modOrigin = FindOrigin(blocks)
print("Model Origin: ", modOrigin)

keys = list(blocks.keys())
for key in keys:
    lpKey = ParseKey(key)
    blocks[key]["coor"] = lSub(blocks[key]["coor"], modOrigin)

r = json.dumps(blocks)
fname = "outPy.txt"
file = open(fname, "w")
file.write(r)
file.close()

print("Number of blocks post conv: ", len(blocks))
#PrintDict(blocks, "size")
#PrintDict(blocks, "coor")












