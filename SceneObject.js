const EPSILON = 0.00000000001;

function SceneObject(gl, program, id, type, root, pos, color, scale, modHandler, extraVec = vec3(1.0, 1.0, 1.0))
{
    this.maxNumDeps = 250;
    this.numDeps = 0;
    this.id = id;
    this.type = type;
    this.root = root;
    this.modHandler = modHandler;
    
    this.position = new vec3(pos[0], pos[1], pos[2]);
    this.angle = [0,0,0];
    this.angVel = [0,0,0];
    this.scale = scale + EPSILON;
    this.exVec = extraVec;
    this.transform =  new Matrix4();
    this.normtransform = new Matrix4();
    
    this.color = color;
    this.reflective = -1.0 - EPSILON;
    
    this.dependencies = [];
    this.posloc = gl.getUniformLocation(program, "objPositions["+this.id+"]");
    this.matloc = gl.getUniformLocation(program, "objMats["+this.id+"]");
    this.matnormloc = gl.getUniformLocation(program, "objNormMats["+this.id+"]");    
    this.colloc = gl.getUniformLocation(program, "objColors["+this.id+"]");
    
    
    //functions
    this.AddDependency = AddBooleanOp;
    this.GetString = GetSObjectString;
    this.GetVerboseString = GetExpandedString;
    this.UpdateShader = UpdateObjShader;
    this.ReinitShader = ReinitObjLocs;
    this.CreateTransform = CreateTransform;
    this.Rotate = RotateObject;
    this.SetRot = SetObjectRotation;
    this.SetAngVel = SetAngularVelocity;
    this.Move = MoveObject;
    this.SetPos = SetObjectPosition;
}

function AddBooleanOp(op, id)
{
    if(this.numDeps < this.maxNumDeps)
    {
        this.dependencies.push(new ObjectRelationship(op, id)) ;
        this.numDeps++;
    }
}

function UpdateObjShader(gl)
{
    this.angle[0] += this.angVel[0];
    this.angle[1] += this.angVel[1];
    this.angle[2] += this.angVel[2];
    this.CreateTransform();

    gl.uniform3fv(this.posloc, this.position);
    gl.uniform4fv(this.colloc, this.color);
    gl.uniformMatrix4fv(this.matloc, false, this.transform.elements);
    gl.uniformMatrix4fv(this.matnormloc, false, this.normtransform.elements);    
}

function ReinitObjLocs(program)
{
    this.posloc = gl.getUniformLocation(program, "objPositions["+this.id+"]");
    this.colloc = gl.getUniformLocation(program, "objColors["+this.id+"]");
    this.matloc = gl.getUniformLocation(program, "objMats["+this.id+"]");  
    this.matnormloc = gl.getUniformLocation(program, "objNormMats["+this.id+"]");       
}

function GetSObjectString()
{
    var pos_str = "vec3(objMats["+ this.id + "]*vec4(p,1.0))";
    var dir_str = "vec3(objMats["+this.id+ "]*vec4(u, 0.0))";
    var col_str = "objColors[" + this.id + "]";
    
    // if its an accel object return Accel function call.
    if(this.root == 2 || this.root == 3)
        return "Accel" + this.id + "(p, u)";
    // return string for primitive distance function call
    if(this.type == 0)
    {        
        return "SphereDist(" + pos_str + ", " + this.scale+", " + this.id + ", " + col_str + ", "+this.reflective+")";
        /*if(this.numDeps == 0 && this.root == 1)
            return "FastSphereDist("+pos_str+", " + dir_str + "," + this.scale+", "+ this.id + ", " + col_str + ", " + this.reflective + ")";           
        else
            return "SphereDist(" + pos_str + ", " + this.scale+", " + this.id + ", " + col_str + ", " + this.reflective + ")";*/
    }   
    else if(this.type == objType.BOX)
        return "BoxDist(" + pos_str + ", vec3("+this.exVec[0]+", "+this.exVec[1]+", "+this.exVec[2]+")*"+this.scale+", " + this.id + ", " + col_str +", "+this.reflective+ ")";    
    else if(this.type == objType.CYLINDER)
        return "CylinderDist(" + pos_str + ", vec2("+this.exVec[0]+", "+this.exVec[1]+")*"+this.scale+", " + this.id + ", " + col_str + ", "+this.reflective+")";      
    else if(this.type == objType.FLOOR)
        return "FloorDist(" + pos_str + ", " + dir_str + ", " + this.id + ", " + col_str + ", " + this.reflective + ")";
    else if(this.type == objType.LEGO)
        return "LegoDist(" + this.id + ", " + pos_str + ", " + this.exVec[0] + ", " + this.exVec[1] + "," + this.scale + ", " + (this.exVec[2] + EPSILON) + ", " + col_str + ", " + this.reflective + ")";       
    else if(this.type >= 100)
        return this.modHandler.GetString(this.type, this.id, this.scale);
        
     return "INVALID SHAPE";
}

//Returns flattened call for the object in order to export for future use
function GetExpandedString()
{
    this.CreateTransform();
    // if its an accel object return Accel function call.
    if(this.root == 2 || this.root == 3)
        return "Accel" + this.id + "(p, u)";
    // return primitive distance function
    if(this.type == 0)
    { 
        return "SphereDist(vec3("+CreateMat4Dec(this.transform )+"*vec4(p,1.0)), " + this.scale + ", " + this.id + ", " +  GetColorString(this.color) + ", " + this.reflective + ")";
    }
    
    else if(this.type == 1)
        return "BoxDist(vec3("+CreateMat4Dec(this.transform )+"*vec4(p,1.0)), " + vecFlatten(this.exVec, 3) + "*" + this.scale + ", " + this.id + ", " + GetColorString(this.color) + ", " + this.reflective + ")";
        
    else if(this.type == 2)
        return "CylinderDist(vec3("+CreateMat4Dec(this.transform )+"*vec4(p,1.0)), " + vecFlatten(this.exVec, 2) + "*" + this.scale + ", " + this.id + ", " + GetColorString(this.color) + ", " + this.reflective + ")";

    else if(this.type == 3)
        return "FloorDist(vec3("+CreateMat4Dec(this.transform )+"*vec4(p,1.0)), vec3("+CreateMat4Dec(this.transform )+"*vec4(u, 0.0))," + this.id + ", " + GetColorString(this.color) + ", " + this.reflective + ")";

    else if(this.type == objType.LEGO)
        return "LegoDist(" + this.id + ", " + "vec3(" + CreateMat4Dec(this.transform) + "*vec4(p,1.0)), " + this.exVec[0] + ", " + this.exVec[1] + "," + this.scale + ", " + (this.exVec[2] + EPSILON) + ", " + GetColorString(this.color) + ", " + this.reflective + ")";
        
    else if(this.type >= 100)
        return this.modHandler.GetString(this.type, this.id);
        
     return "INVALID SHAPE";  
}

function vecFlatten(v, dim)
{
    values = ""
    for(var i = 0; i < dim; i++){
        values += v[i]
        if(i < dim - 1)
            values += ", "
    }

    return "vec" + dim + "(" + values + ")"
}

//returns string for expanded matrix
function CreateMat4Dec(trans)
{
    transform = trans.elements;
    str = "mat4(vec4(" + transform[0] + "," + transform[1]+ "," + transform[2] + "," + transform[3] + ")," 
            +"vec4(" + transform[4] + "," + transform[5]+ "," + transform[6] + "," + transform[7] + "),"
            +"vec4(" + transform[8] + "," + transform[9]+ "," + transform[10] + "," + transform[11] + "),"
            +"vec4(" + transform[12] + "," + transform[13]+ "," + transform[14] + "," + transform[15] + "))";
    return str;
}

function GetPosString(p)
{
    return "vec3("+p[0]+", "+p[1]+", "+p[2]+")";
}

function GetColorString(p)
{
    return "vec4("+p[0]+", "+p[1]+", "+p[2]+", "+p[3]+")";
}

//Create ray transforms for the object as well as the transform for normals
function CreateTransform()
{
    tiMat = new Matrix4();
    rxiMat = new Matrix4();
    ryiMat = new Matrix4();
    rziMat = new Matrix4();
    
    tiMat.translate(-1*this.position[0], -1*this.position[1], -1*this.position[2]);
    rxiMat.rotate(-1*this.angle[0], 1, 0, 0);
    ryiMat.rotate(-1*this.angle[1], 0, 1, 0);
    rziMat.rotate(-1*this.angle[2], 0, 0, 1);
    rziMat.multiply(ryiMat);
    rziMat.multiply(rxiMat);
    rziMat.multiply(tiMat);
    this.transform.set(rziMat);
	//this.normtransform.setIdentity();
    this.normtransform.set(rziMat);
    this.normtransform.transpose();
}

function RotateObject(theta, axisIndex)
{
    if(axisIndex < 0 || axisIndex > 2)
        return -1.1234567;
    this.angle[axisIndex] += theta;
    return this.angle[axisIndex];
}

function SetAngularVelocity(theta, axisIndex)
{
    if(axisIndex < 0 || axisIndex > 2)
        return -1.1234567;
    this.angVel[axisIndex] = theta;
    return 1;
}

function SetObjectRotation(thetax, thetay, thetaz)
{
    this.angle[0] = thetax;
    this.angle[1] = thetay;
    this.angle[2] = thetaz;
}

function MoveObject(vel)
{
    this.position[0] += vel[0];
    this.position[1] += vel[1];
    this.position[2] += vel[2];
}

function SetObjectPosition(x,y,z)
{
    this.position[0] = x;
    this.position[1] = y;
    this.position[2] = z;
}

function ObjectRelationship(op, id)
{
    this.op = op;
    this.id = id;
}